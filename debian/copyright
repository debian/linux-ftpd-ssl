Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Comment: This package was split from netstd by Herbert Xu herbert@debian.org
Source: ftp://ftp.uk.linux.org/pub/linux/Networking/netkit/
        ftp://ftp.uni-mainz.de/pub/software/security/ssl/SSL-MZapps/linux-ftpd-0.17+ssl-0.3.diff.gz

Files: *
Copyright: 1985-1994 The Regents of the University of California.
 All rights reserved.
License: BSD-4-clause-UC

Files: debian/patches/016-family_independence.diff
       debian/patches/020-support_ipv6.diff
Copyright: 2010, Mats Erik Andersson <debian@gisladisker.se>
License: BSD-4-clause-UC
Comment: The license of the original software applies to these patches.

Files: debian/patches/500-ssl.diff
License: BSD-4-clause-UC and linux-ftpd-ssl

Files: support/setproctitle.c
Copyright: 1983, 1995 Eric P. Allman
           1988, 1993 The Regents of the University of California.  All rights reserved.
License: BSD-4-clause-UC

Files: ftpd/logutmp.c
Copyright: 1988, 1993 The Regents of the University of California.  All rights reserved.
           1996, Jason Downs.  All rights reserved.
License: BSD-4-clause-UC

Files: ftpd/ftpusers.5
Copyright: 1994 Peter Tobias (tobias@server.et-inf.fho-emden.de)
License: GPL
 This file may be distributed under the GNU General Public License.
Comment:
 See /usr/share/common-licenses/GPL for the complete license text.

Files: support/setproctitle.3
Copyright: 1994, 1995 Christopher G. Demetriou
 All rights reserved.
License: BSD-4-clause-Demetriou
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. All advertising materials mentioning features or use of this software
    must display the following acknowledgement:
      This product includes software developed by Christopher G. Demetriou
      for the NetBSD Project.
 4. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: BSD-4-clause-UC
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. <deleted>
 4. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: linux-ftpd-ssl
 The modifications to support SSLeay were done by Tim Hudson
 tjh@cryptsoft.com
 .
 You can do whatever you like with these patches except pretend that
 you wrote them.

This is a simple procedure to arrive at a SSL certificate
allowing a quick start of ftpd-ssl.

1. Generate an RSA key file:

  $ openssl genrsa -out /run/shm/ftpd.key

2. Generate a self-signed certificate:

  $ openssl req -config /etc/ftpd-ssl/openssl.cnf \
                -new  -x509  -days 100 \
                -key /run/shm/ftpd.key \
		-out /etc/ftpd-ssl/ftpd.tmp

  A fair number of question will have to be answered in order
  to produce a valid certificate.

3. Set up a combined file at the default location:

  $ cat /run/shm/ftpd.tmp /run/shm/ftpd.key > /run/shm/ftpd.pem

  $ sudo cp /run/shm/ftpd.pem /etc/ftpd-ssl/

  $ sudo chmod 0600 /etc/ftpd-ssl/ftpd.pem

  $ rm /run/shm/ftpd.*

Now the ftpd should be able to start properly.

Description: Missing use of SSL protected stream.
 The commands HELP and STAT are not directing all their
 output via the SSL mechanisms, instead sending data to
 stdout.  This appears as data loss to the client, or as
 errors during SSL protected transmission.
 .
 Four particular problem cases are notable:
  *  STAT
  *  STAT path
  *  HELP
  *  HELP SITE

Author: Mats Erik Andersson <debian@gisladisker.se>
Bug-Debian: http://bugs.debian.org/774454
Last-Update: 2015-01-06

--- linux-ftpd-ssl.orig/ftpd/ftpcmd.y	2015-01-03 19:38:41.000000000 +0100
+++ linux-ftpd-ssl/ftpd/ftpcmd.y	2015-01-06 22:54:07.636050827 +0100
@@ -1455,10 +1455,11 @@ static void help(struct tab *ctab, char
 			width = len;
 		NCMDS++;
 	}
-	width = (width + 8) &~ 7;
+	width = (width + 8) &~ 7;	/* Multiple of eight.  */
 	if (s == 0) {
 		int i, j, w;
 		int columns, lines;
+		char mesg[80];		/* Actually 74 would suffice.  */
 
 		lreply(214, "The following %scommands are recognized %s.",
 		    type, "(* =>'s unimplemented)");
@@ -1466,23 +1467,33 @@ static void help(struct tab *ctab, char
 		if (columns == 0)
 			columns = 1;
 		lines = (NCMDS + columns - 1) / columns;
+
 		for (i = 0; i < lines; i++) {
-			printf("   ");
+			mesg[0] = 0;
+			strcat(mesg, "   ");
 			for (j = 0; j < columns; j++) {
 				c = ctab + j * lines + i;
-				printf("%s%c", c->name,
-					c->implemented ? ' ' : '*');
+				strcat(mesg, c->name);
+				strcat(mesg, c->implemented ? " " : "*");
 				if (c + lines >= &ctab[NCMDS])
 					break;
 				w = strlen(c->name) + 1;
 				while (w < width) {
-					putchar(' ');
+					strcat(mesg, " ");
 					w++;
 				}
 			}
-			printf("\r\n");
+			strcat(mesg, "\r\n");
+#ifdef USE_SSL
+			if (ssl_active_flag)
+				SSL_write(ssl_con, mesg, strlen(mesg));
+			else
+#endif /* USE_SSL */
+			printf("%s", mesg);
 		}
+#ifndef USE_SSL
 		(void) fflush(stdout);
+#endif /* !USE_SSL */
 		reply(214, "Direct comments to ftp-bugs@%s.", hostname);
 		return;
 	}
--- linux-ftpd-ssl.orig/ftpd/ftpd.c	2015-01-03 19:38:41.000000000 +0100
+++ linux-ftpd-ssl/ftpd/ftpd.c	2015-01-06 23:13:43.152038999 +0100
@@ -2366,10 +2366,17 @@ static int receive_data(FILE *instr, FIL
 			goto file_err;
 		transflag = 0;
 		if (bare_lfs) {
+			const char *mesg = "   File may not have transferred correctly.\r\n";
+
 			lreply(226,
 		"WARNING! %d bare linefeeds received in ASCII mode",
 			    bare_lfs);
-		(void)printf("   File may not have transferred correctly.\r\n");
+#ifdef USE_SSL
+			if (ssl_active_flag)
+				SSL_write(ssl_con, mesg, strlen(mesg));
+			else
+#endif /* USE_SSL */
+				(void)printf("%s", mesg);
 		}
 		return (0);
 	default:
@@ -2411,8 +2418,21 @@ void statfilecmd(char *filename)
 				(void) ftpd_pclose(fin);
 				return;
 			}
+#ifdef USE_SSL
+			if (ssl_active_flag)
+				SSL_write(ssl_con, "\r", 1);
+			else
+#endif /* USE_SSL */
 			(void) putc('\r', stdout);
 		}
+#ifdef USE_SSL
+		if (ssl_active_flag) {
+			char chbuf[1];
+
+			chbuf[0] = c;
+			SSL_write(ssl_con, chbuf, 1);
+		} else
+#endif /* USE_SSL */
 		(void) putc(c, stdout);
 	}
 	(void) ftpd_pclose(fin);
@@ -2423,63 +2443,172 @@ void statcmd(void)
 {
 	struct sockaddr *sn;
 	char ipaddr[INET6_ADDRSTRLEN];
+	char *mesg, msgbuf[128];
+	int len;
 
 	lreply(211, "%s FTP server status:", hostname, version);
-	printf("     %s\r\n", version);
-	printf("     Connected to %s", remotehost);
+	len = sprintf(msgbuf, "     %s\r\n", version);
+#ifdef USE_SSL
+	if (ssl_active_flag)
+		SSL_write(ssl_con, msgbuf, len);
+	else
+#endif /* USE_SSL */
+	printf("%s", msgbuf);
+	len = snprintf(msgbuf, sizeof msgbuf,
+		       "     Connected to %s", remotehost);
+#ifdef USE_SSL
+	if (ssl_active_flag)
+		SSL_write(ssl_con, msgbuf,
+			  (len < sizeof msgbuf) ? len : sizeof msgbuf);
+	else
+#endif /* USE_SSL */
+	printf("%s", msgbuf);
 	if (!isdigit(remotehost[0])) {
 		(void) getnameinfo((struct sockaddr *) &his_addr,
 				sizeof(struct sockaddr_storage),
 				ipaddr, sizeof(ipaddr), NULL, 0,
 				NI_NUMERICHOST);
-		printf(" (%s)", ipaddr);
+		len = sprintf(msgbuf, " (%s)", ipaddr);
+#ifdef USE_SSL
+		if (ssl_active_flag)
+			SSL_write(ssl_con, msgbuf, len);
+		else
+#endif /* USE_SSL */
+		printf("%s", msgbuf);
 	}
+#ifdef USE_SSL
+	if (ssl_active_flag)
+		SSL_write(ssl_con, "\r\n", 2);
+	else
+#endif /* USE_SSL */
 	printf("\r\n");
+
+	/* Exactly one of four possible messages, put it in MSG.  */
 	if (logged_in) {
 		if (guest)
-			printf("     Logged in anonymously\r\n");
-		else
-			printf("     Logged in as %s\r\n", pw->pw_name);
+			mesg = "     Logged in anonymously\r\n";
+		else {
+			len = snprintf(msgbuf, sizeof msgbuf,
+				       "     Logged in as %s\r\n",
+				       pw->pw_name);
+			mesg = msgbuf;
+		}
 	} else if (askpasswd)
-		printf("     Waiting for password\r\n");
+		mesg = "     Waiting for password\r\n";
+	else
+		mesg = "     Waiting for user name\r\n";
+
+#ifdef USE_SSL
+	if (ssl_active_flag)
+		SSL_write(ssl_con, mesg, strlen(mesg));
 	else
-		printf("     Waiting for user name\r\n");
-	printf("     TYPE: %s", typenames[type]);
-	if (type == TYPE_A || type == TYPE_E)
-		printf(", FORM: %s", formnames[form]);
-	if (type == TYPE_L)
+#endif /* USE_SSL */
+	printf("%s", mesg);
+
+	len = sprintf(msgbuf, "     TYPE: %s", typenames[type]);
+#ifdef USE_SSL
+	if (ssl_active_flag)
+		SSL_write(ssl_con, msgbuf, len);
+	else
+#endif /* USE_SSL */
+	printf("%s", msgbuf);
+
+	if (type == TYPE_A || type == TYPE_E) {
+		len = sprintf(msgbuf, ", FORM: %s", formnames[form]);
+#ifdef USE_SSL
+		if (ssl_active_flag)
+			SSL_write(ssl_con, msgbuf, len);
+		else
+#endif /* USE_SSL */
+		printf("%s", msgbuf);
+	}
+	if (type == TYPE_L) {
+		len = sprintf(msgbuf,
 #if CHAR_BIT == 8
-		printf(" %d", CHAR_BIT);
+			       " %d", CHAR_BIT
 #else
-		printf(" %d", bytesize);	/* need definition! */
+			       " %d", bytesize	/* needs definition! */
 #endif
-	printf("; STRUcture: %s; transfer MODE: %s\r\n",
-	    strunames[stru], modenames[mode]);
-	if (data != -1)
-		printf("     Data connection open\r\n");
+			      );
+#ifdef USE_SSL
+		if (ssl_active_flag)
+			SSL_write(ssl_con, msgbuf, len);
+		else
+#endif /* USE_SSL */
+		printf("%s", msgbuf);
+	}
+
+	len = snprintf(msgbuf, sizeof msgbuf,
+		       "; STRUcture: %s; transfer MODE: %s\r\n",
+		       strunames[stru], modenames[mode]);
+#ifdef USE_SSL
+	if (ssl_active_flag)
+		SSL_write(ssl_con, msgbuf, len);
+	else
+#endif /* USE_SSL */
+	printf("%s", msgbuf);
+
+	if (data != -1) {
+		mesg = "     Data connection open\r\n";
+#ifdef USE_SSL
+		if (ssl_active_flag)
+			SSL_write(ssl_con, mesg, strlen(mesg));
+		else
+#endif /* USE_SSL */
+		printf("%s", mesg);
+	}
 	else if (pdata != -1) {
-		printf("     in Passive mode");
+		mesg = "     in Passive mode";
 		sn = (struct sockaddr *) &pasv_addr;
 		goto printaddr;
 	} else if (usedefault == 0) {
-		printf("     PORT");
+		mesg = "     PORT";
 		sn = (struct sockaddr *) &data_dest;
+
 printaddr:
+		/* A constant message was prepared in two cases.  */
+#ifdef USE_SSL
+		if (ssl_active_flag)
+			SSL_write(ssl_con, mesg, strlen(mesg));
+		else
+#endif /* USE_SSL */
+			printf("%s", mesg);
+
 #define UC(b) (((int) b) & 0xff)
-		if (sn->sa_family == AF_INET6)
-			printf(" (|||%d|)\r\n", get_port(sn));
-		else {
+		if (sn->sa_family == AF_INET6) {
+			len = sprintf(msgbuf, " (|||%d|)\r\n",
+				      get_port(sn));
+#ifdef USE_SSL
+			if (ssl_active_flag)
+				SSL_write(ssl_con, msgbuf, len);
+			else
+#endif /* USE_SSL */
+			printf("%s", msgbuf);
+		} else {
 			uint32_t ip4ad = get_ipv4address(sn);
 			in_port_t port = get_port(sn);
 
-			printf(" (%d,%d,%d,%d,%d,%d)\r\n",
+			len = sprintf(msgbuf, " (%d,%d,%d,%d,%d,%d)\r\n",
 				UC(ip4ad >> 24), UC(ip4ad >> 16),
 				UC(ip4ad >> 8), UC(ip4ad),
 				UC(port >> 8), UC(port));
+#ifdef USE_SSL
+			if (ssl_active_flag)
+				SSL_write(ssl_con, msgbuf, len);
+			else
+#endif /* USE_SSL */
+			printf("%s", msgbuf);
 		}
 #undef UC
-	} else
-		printf("     No data connection\r\n");
+	} else {
+		mesg = "     No data connection\r\n";
+#ifdef USE_SSL
+		if (ssl_active_flag)
+			SSL_write(ssl_con, mesg, strlen(mesg));
+		else
+#endif /* USE_SSL */
+		printf("%s", mesg);
+	}
 	reply(211, "End of status");
 }
 

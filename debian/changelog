linux-ftpd-ssl (0.17.36+really0.17-3) unstable; urgency=medium

  * QA upload.
  * add gbp.conf
  * add Vcs fields to debian/control
  * Use wrap-and-sort -satb for debian control files
  * Trim trailing whitespace.
  * Update renamed lintian tag names in lintian overrides.
  * Fix day-of-week for changelog entries 0.17.18+0.3-9, 0.17.18+0.3-2.
  * add Rules-Requires-Root field
  * update debhelper-compat to 12

 -- Christoph Martin <martin@uni-mainz.de>  Tue, 07 May 2024 16:03:34 +0200

linux-ftpd-ssl (0.17.36+really0.17-2) unstable; urgency=medium

  * QA upload.
  * Don't conflict with ftpd.
  * Set Priority optional (was: extra).

 -- Bastian Germann <bage@debian.org>  Thu, 08 Sep 2022 11:14:01 +0200

linux-ftpd-ssl (0.17.36+really0.17-1) unstable; urgency=medium

  * QA upload.
  * Ignore cert file with -z nossl set.
  * Drop outdated README.SSL.
  * d/copyright: Convert to machine-readable format.
    + MIT-CMU is not applicable (nowhere in upstream code).

 -- Bastian Germann <bage@debian.org>  Wed, 07 Sep 2022 09:39:57 +0200

linux-ftpd-ssl (0.17.36+0.3-2.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Tweak cmake patches to link correctly. Closes: #925768
  * Drop obsolete libssl1.0-dev dependency. Closes: #917348
    - drop version constraint, satisfied already since o-o-stable

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 12 Aug 2019 18:39:22 +0200

linux-ftpd-ssl (0.17.36+0.3-2.1) unstable; urgency=high

  * Non-maintainer upload
  * Use cmake as build system. Closes: #912123

 -- Christoph Biedl <debian.axhn@manchmal.in-ulm.de>  Sun, 24 Feb 2019 14:39:30 +0100

linux-ftpd-ssl (0.17.36+0.3-2) unstable; urgency=low

  * Adaption to libssl of recent version.
    + debian/patches/580-recent_libssl.diff: New file.
    + debian/control: Reverse order between libssl-dev and libssl1.0-dev,
      remove versioning of libssl-dev.
  * Fulfill minimal requirements in RFC 2228.  Accept CA-files, key files,
    cipher selection, and chained certificates.  Full verification of any
    certificate is possible.
    + Immediately forbid SSLv2 while initializing SSL.
    + debian/patches/600-better_conformity.diff: New file.

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Tue, 24 Jan 2017 15:35:15 +0100

linux-ftpd-ssl (0.17.36+0.3-1) unstable; urgency=low

  * Update to source version 0.17-36 of linux-ftpd.
  * Allow build dependency on libssl1.0-dev, or libssl-dev (<< 1.1.0~).
    Closes: #828424
  * Silence pedantic complaints on archaic code source.
    + debian/source/lintian-overrides: New file.

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Sat, 17 Dec 2016 15:41:43 +0100

linux-ftpd (0.17-36) unstable; urgency=low

  * Step up to Standards version 3.9.8, no changes.
  * Increase hardening level.

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Tue, 06 Dec 2016 16:09:38 +0100

linux-ftpd-ssl (0.17.35+0.3-2) unstable; urgency=medium

  * NLST of empty directory results in segfault.
    + debian/patches/500-ssl.diff: Updated. (Closes: #788331)

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Thu, 11 Jun 2015 19:25:49 +0200

linux-ftpd-ssl (0.17.35+0.3-1) unstable; urgency=low

  * Update to source version 0.17-35 of linux-ftpd.
    + debian/control: Set Build-Depends to '>= 9' for debhelper.
    + debian/compat: Update.
  * Bring in package changes from experimental to unstable.

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Mon, 27 Apr 2015 15:29:17 +0200

linux-ftpd (0.17-35) unstable; urgency=low

  * Interchange maintainer and uploader.
  * Update Standards to 3.9.6.
    + debian/rules: Add missing targets build-arch, build-indep.
    + debian/control: Set Build-Depends to '>= 9' for debhelper.
    + debian/compat: Update.
  * debian/rules: Use the same build flags CDEFS and LDDEFS as is in
    use by the variant linux-ftpd-ssl.
  * debian/postinst: Mistyped informational text. (Closes: #741111)

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Mon, 27 Apr 2015 14:31:44 +0200

linux-ftpd-ssl (0.17.34+0.3-1) experimental; urgency=low

  * New maintainer and new uploader.
  * Update Standards to 3.9.6
    + debian/rules: Add missing targets build-arch, build-indep.
  * Activate hardening flags like linux-ftpd does.
    + debian/rules: Update is based on `dpkg-buildflags'.
  * Update to source version 0.17-34 of linux-ftpd.
    + debian/rules: Call dh_installexamples.
    + debian/postinst: Adapt message to `ftpd-ssl'.
    + debian/ftpd-ssl.examples: Renamed file.
  * Missing use of SSL protected stream. (Closes: #774454)
    + debian/patches/570-redirect_ssl_output.diff: New file.
  * debian/postinst: Add a hint when an SSL certificate is missing.
  * debian/watch: Correction to URL.
  * Include a descriptive text about making test certificates.
    + debian/simple-cert.txt: New file.
    + debian/ftpd-ssl.examples: Mention the previous file.

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Wed, 07 Jan 2015 22:25:56 +0100

linux-ftpd (0.17-34) unstable; urgency=low

  * Drop unneeded libcrypt linking:
    + debian/patches/040-refine_config.diff: New file.
  * Support GNU/Hurd:
    + debian/patches/044-support_gnu_hurd.diff: New file.
    + debian/control: Mention GNU/Hurd in package description.
  * Activate hardening flags. Suggested by Moritz Muehlenhoff.
    + debian/rules: Use 'dpkg-buildflags'. (Closes: #656005)
  * Bump Standards to 3.9.3.

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Fri, 30 Mar 2012 00:54:44 +0200

linux-ftpd-ssl (0.17.33+0.3-1) unstable; urgency=low

  * Update to linux-ftpd 0.17-33.
  * Drop hard-coded dependency on libssl0.9.8.
  * Standards-Version: 3.9.1 (no changes).

 -- Ian Beckwith <ianb@debian.org>  Wed, 20 Apr 2011 02:46:14 +0100

linux-ftpd (0.17-33) unstable; urgency=low

  * Correct implementation of EPSV for IPv4.
    + debian/patches/020-support_ipv6.diff: Updated file.
  * [lintian] Addition of DEP-3 header:
    + debian/patches/030-manpage_typos.diff: Description added.
  * debian/control: Spelling of 'kFreeBSD'.
  * Bumped Standards to 3.9.1.

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Wed, 06 Apr 2011 22:34:30 +0200

linux-ftpd-ssl (0.17.32+0.3-1) unstable; urgency=low

  * Update to linux-ftpd 0.17-32.
  * Standards-Version: 3.9.0 (no changes).

 -- Ian Beckwith <ianb@debian.org>  Sun, 25 Jul 2010 01:32:41 +0100

linux-ftpd (0.17-32) unstable; urgency=low

  * Fixed minor typos in ftpd.8.
  * Renamed patches to avoid future clashes with linux-ftpd-ssl.
  * Bumped Standards to 3.9.0

 -- Alberto Gonzalez Iniesta <agi@inittab.org>  Thu, 08 Jul 2010 13:28:07 +0200

linux-ftpd-ssl (0.17.31+0.3-1) unstable; urgency=low

  * Update to linux-ftpd 0.17-31
    + IPv6 support! Thanks to Mats Erik Andersson
      <mats.andersson@gisladisker.se>.
  * Convert to format 3.0 (quilt).
  * Extract source changes to patches in debian/patch/, annotate
    patches in DEP3 format. SSL patches start at 500, patches
    below that are imported unchanged from the debian linux-ftpd
    package.
  * debian/rules: add -fno-strict-align to CFLAGS.
  * ftpd.8: typo fixes.
  * Switch to debhelper compat level 7.
  * debian/watch: check for new debian package of linux-ftpd.
  * Update maintainer email.
  * Remove DM-Upload-Allowed flag.
  * Standards-Version: 3.8.4 (no changes).

 -- Ian Beckwith <ianb@debian.org>  Mon, 14 Jun 2010 02:14:27 +0100

linux-ftpd (0.17-31) unstable; urgency=low

  [Mats Erik Andersson]

  * debian/control: Expanded description.
  * debian/copyright: Added explicit license for the two IPv6-patches.
  * Describe effects of recent changes on three inetd-servers.
    + debian/NEWS: Short text addition.
    + debian/README.Debian: Added an extended discussion.
  * FTBFS GNU/kfreebsd, GNU/Hurd:
    * debian/patches/26-support_glibc_bsd_and_gnu.diff: New file.
  * debian/{postinst,prerm}: Use option '--multi' with 'update-inetd'.

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Tue, 25 May 2010 20:05:34 +0200

linux-ftpd (0.17-30) unstable; urgency=low

  * Migration to format "3.0 (quilt)".
    + Identified source code patches:
    + debian/patches/01-from_hamm.diff
    + debian/patches/02-from_sarge.diff
    + debian/patches/03-from_etch.diff
    + debian/patches/10-ftpd_csrf.diff
  * Standard 3.8.4, debhelper compatibility 7:
    + Using dh_prep.
  * debian/control:
    + Build depend includes ${misc:Depends} and 'debhelper (>= 7).
    + Source homepage stanza pointing to download site.
  * debian/watch: New file.
  * debian/postinst: Suppress error message if '/etc/inetd.conf' is missing.
    + Print a hint to the example xinetd configuration, if applicable.
  * debian/ftpd.examples:
    + debian/ftpd.xinetd: Configuration for use with xinetd.
  * Prepare migration to IPv6 support:
    + debian/rules: Use compiler flag '-fno-strict-aliasing'.
    + debian/patches/14-adjust_infrastruct.diff: New file.
    + debian/patches/16-family_independence.diff: New file.
  * Implement active IPv6 support: (Closes: #580251)
    + debian/patches/20-support_ipv6.diff: New file.
    + debian/NEWS: New file.
    + debian/README.Debian: A short explanation is added.
  * Uninitialized va_list causes segmentation faults on amd64:
    + debian/patches/24-failing_va_list.diff: New file.
  * [lintian] maintainer-script-without-set-e:
    + debian/{preinst,prerm,postinst,postrm} modified.
  * [lintian] debian-rules-ignores-make-clean-error:
    + debian/rules modified.
  * agi: Added Mats Erik Andersson as Uploader.

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Sun, 09 May 2010 16:17:20 +0200

linux-ftpd (0.17-29) unstable; urgency=high

  * Ian Beckwith:
    - Patch to fix cross-site request forgery (CSRF) attacks.
      CVE-2008-4247 (Closes: #500278)
  * Updated package description. (Closes: #493433)

 -- Alberto Gonzalez Iniesta <agi@inittab.org>  Fri, 17 Oct 2008 20:34:17 +0200

linux-ftpd (0.17-28) unstable; urgency=low

  * Patch postrm/postinst files to handle update-inetd better.
    Thanks Ian Beckwith for the patch. (Closes: #482912)

 -- Alberto Gonzalez Iniesta <agi@inittab.org>  Tue, 22 Jul 2008 17:13:19 +0200

linux-ftpd-ssl (0.17.27+0.3-3) unstable; urgency=high

  * Fix command line split CSRF (Closes: #500518)
    Addresses CVE-2008-4242 and CVE-2008-4247.

 -- Ian Beckwith <ianb@erislabs.net>  Thu, 16 Oct 2008 22:13:23 +0100

linux-ftpd-ssl (0.17.27+0.3-2) unstable; urgency=low

  * Create debian/NEWS with details of openssl problems
    and key rollover.
  * Explicitly depend/build-depend on fixed openssl.
  * ftpd/ftpd.c: Set default ssl key/cert file to /etc/ftpd-ssl/ftpd.pem.
  * postinst:
    + On new installs don't bother with -z cert and -z key, as we
      can now use the defaults.
    + Revert to update-inetd logic from linux-ftpd.
  * postrm: Only remove inetd entry on purge, and only if it is commented out.
  * Add private copy of openssl.cnf from openssl 0.9.8g-10 (See #372105).
  * debian/control:
    + Added DM-Upload-Allowed: yes.

 -- Ian Beckwith <ianb@erislabs.net>  Sun, 25 May 2008 19:24:55 +0100

linux-ftpd-ssl (0.17.27+0.3-1) unstable; urgency=low

  * New maintainer (Closes: #476132).
  * Update to linux-ftpd 0.17-27.
  * Fix all warnings in source.
  * ftpd.8: Document SSL options, fix typo.
  * debian/control:
    + Standards-Version: 3.7.3 (no changes).
  * debian/rules:
    + Support DEB_BUILD_OPTIONS=noopt.
    + Added -Wl,-z,defs -Wl,--as-needed to LDFLAGS.
    + Fixed make distclean invocation.
    + Removed unused install-stamp.
    + Removed unnecessary debhelper calls.
  * debian/postinst: worked round checkbashism false positive.
  * debian/postrm: fixed wording of error message.
  * debian/copyright: added copyright info for SSL patches.
  * Bumped debhelper compat level to 5.
  * Use linux-ftpd_0.17.orig.tar.gz as our orig.tar.gz.

 -- Ian Beckwith <ianb@erislabs.net>  Wed, 23 Apr 2008 01:06:35 +0100

linux-ftpd (0.17-27) unstable; urgency=low

  * Ignore missing ../MCONFIG in clean target. (Closes: #436720)

 -- Alberto Gonzalez Iniesta <agi@inittab.org>  Wed, 08 Aug 2007 20:49:22 +0200

linux-ftpd (0.17-26) unstable; urgency=low

  * Removed Depends on update-inetd, since it should be provided by
    inetd-superserver.

 -- Alberto Gonzalez Iniesta <agi@inittab.org>  Tue, 07 Aug 2007 18:02:11 +0200

linux-ftpd (0.17-25) unstable; urgency=low

  * Condition the call to update-inetd in postrm. (Closes: #416745)
  * Moved netbase dependency to openbsd-inetd | inet-superserver.
  * Added Depends on update-inetd
  * Moved to debhelper compat 4. (Removed debian/conffiles)
  * Bumped Standards-Version to 3.7.2.2. No change

 -- Alberto Gonzalez Iniesta <agi@inittab.org>  Sat, 31 Mar 2007 19:43:37 +0200

linux-ftpd (0.17-24) unstable; urgency=low

  * pam.d/ftpd. Updated PAM configuration to used common-* files.
    Thanks a lot Pierre Gaufillet for the patch (Closes: #308651)

 -- Alberto Gonzalez Iniesta <agi@inittab.org>  Thu, 14 Dec 2006 12:14:53 +0100

linux-ftpd (0.17-23) unstable; urgency=high

  * Urgency set due to security fix.
  * Corrected typo in patch used in previous upload that
    made the server run some commands with EGID 'root'.
    Thanks to Matt Power (for finding out) and
    Stefan Cornelius from Gentoo (for warning me).

 -- Alberto Gonzalez Iniesta <agi@inittab.org>  Sat, 25 Nov 2006 18:54:59 +0100

linux-ftpd (0.17-22) unstable; urgency=high

  * Fixing two security bugs:
    - Fixed ftpd from doing chdir while running as root.
      (Closes: #384454) Thanks a lot to Paul Szabo for finding out
      and the patch.
    - Check the return value from setuid calls to avoid running
      code as root. Thanks Paul Szabo for the patch.

 -- Alberto Gonzalez Iniesta <agi@inittab.org>  Fri, 15 Sep 2006 13:14:25 +0200

linux-ftpd (0.17-21) unstable; urgency=low

  * Patched ftpcmd.y to allow building on amd64 with gcc-4.0.
    Thanks Andreas Jochens for the patch. (Closes: #300245)

 -- Alberto Gonzalez Iniesta <agi@inittab.org>  Sun, 10 Jul 2005 08:48:19 +0200

linux-ftpd (0.17-20) unstable; urgency=low

  * New maintainer.

 -- Alberto Gonzalez Iniesta <agi@inittab.org>  Sat, 12 Mar 2005 12:07:29 +0100

linux-ftpd (0.17-19) unstable; urgency=low

  * Add -n option to log numeric IPs rather than doing reverse lookup (for
    improved log forensics in the event an attacker has control of their
    reverse DNS.).  Thanks Dean Gaudet. (Closes: #258369)
  * Fix Build-Depends to avoid relying on virtual package 'libpam-dev'
    exclussively.
  * Convert changelog to UTF-8.

 -- Robert Millan <rmh@debian.org>  Wed, 11 Aug 2004 22:08:44 +0200

linux-ftpd-ssl (0.17.18+0.3-9.1) unstable; urgency=high

  * Non-maintainer upload by the testing-security team.
  * Fix remote denial of service cause by passing an
    uninitialized file stream to fopen().Initializing file
    with NULL and checking for NULL before calling fclose()
    fixes this (CVE-2007-6263; Closes: #454733).

 -- Nico Golde <nion@debian.org>  Mon, 10 Dec 2007 18:34:40 +0100

linux-ftpd-ssl (0.17.18+0.3-9) unstable; urgency=low

   * debian/postrm: invoke update-inetd if it is present.
     (Closes: #416746)

 -- Cai Qian <caiqian@debian.org>  Tue, 03 Apr 2007 00:00:00 +0800

linux-ftpd-ssl (0.17.18+0.3-8) unstable; urgency=low

   * debian/control: dropped update-inetd,as netbase has already
     provided it.
   * debian/postrm: replaced update-inetd with sed. (Closes: #416746)
   * debian/conffile: dropped it.

 -- Cai Qian <caiqian@debian.org>  Mon, 2 Apr 2007 16:00:00 +0800

linux-ftpd-ssl (0.17.18+0.3-7) unstable; urgency=low

   * debian/control: depends on update-inetd. (Closes: #351204, #416746)

 -- Cai Qian <caiqian@debian.org>  Fri, 30 Mar 2007 16:00:00 +0800

linux-ftpd-ssl (0.17.18+0.3-6) unstable; urgency=low

  * Move the certificate file to /etc/ftpd-ssl. Patch from James Westby
    <jw+debian@jameswestby.net>. (Closes: #368420)
  * Remove debian/conffile

 -- Cai Qian <caiqian@debian.org>  Sat, 01 July 2006 12:27:01 +0100

linux-ftpd-ssl (0.17.18+0.3-5) unstable; urgency=high

  * applied security patch for CVE-2005-3524. (Closes: #339074)

 -- Cai Qian <caiqian@debian.org>  Fri, 18 Nov 2005 17:27:01 +0000

linux-ftpd-ssl (0.17.18+0.3-4) unstable; urgency=low

  * applied gcc4/amd64 patch by Andreas Jochens (Closes: #300247)

 -- Cai Qian <caiqian@debian.org>  Wed, 28 Sep 2005 01:04:00 +0100

linux-ftpd-ssl (0.17.18+0.3-3) unstable; urgency=low

  * encoded changelog to UTF-8
  * followed debhelper V4

 -- Cai Qian <caiqian@gnome.org>  Mon, 15 Nov 2004 22:45:00 +0800

linux-ftpd-ssl (0.17.18+0.3-2) unstable; urgency=low

  * New maintainer (Closes: #250711)
  * Change debian/rules to fix missing ftpd.8

 -- Cai Qian <caiqian@gnome.org>  Mon, 27 Sep 2004 01:45:00 +0800

linux-ftpd-ssl (0.17.18+0.3-1) unstable; urgency=low

  * Bring linux-ftpd in line with current netkit-telnet
  * Build for sid/sarge
  * Make ftp-ssl protocol compatible with
    http://www.ietf.org/internet-drafts/draft-murray-auth-ftp-ssl-09.txt
    or http://www.ietf.org/rfc/rfc2228.txt. This can break compatiblility
    with older ftp-ssl (closes: #154138)

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Thu, 27 May 2004 15:00:27 +0200

linux-ftpd (0.17-18) unstable; urgency=low

  * New maintainer. (Closes: #249709)
    - control (Maintainer): Set myself.

 -- Robert Millan <rmh@debian.org>  Wed, 19 May 2004 02:09:10 +0200

linux-ftpd (0.17-17) unstable; urgency=low

  * Documented the need for libnss_files.so.2 (closes: #241687).

 -- Herbert Xu <herbert@debian.org>  Sat, 24 Apr 2004 17:48:37 +1000

linux-ftpd (0.17-16) unstable; urgency=low

  * Fixed ftpd entry existence test in postinst.
  * Added missing -q for last grep in postinst.
  * Removed debconf message about globbing attacks.
  * Fixed type-punning warning in pam_doit.
  * Removed stamp files for build and install.
  * Fixed const cast warning in ftpcmd.y.

 -- Herbert Xu <herbert@debian.org>  Sat, 21 Jun 2003 14:20:35 +1000

linux-ftpd (0.17-15) unstable; urgency=low

  * Removed description of bogus -p option (closes: #180652).

 -- Herbert Xu <herbert@debian.org>  Thu,  6 Mar 2003 20:26:16 +1100

linux-ftpd (0.17-14) unstable; urgency=low

  * Added Spanish debconf translation (Carlos Valdivia Yagüe, closes: #143956).
  * Call ls without -g (closes #156992).

 -- Herbert Xu <herbert@debian.org>  Sun, 25 Aug 2002 10:09:07 +1000

linux-ftpd (0.17-13) unstable; urgency=low

  * Added Russian debconf translation (Ilgiz Kalmetev, closes: #135840).

 -- Herbert Xu <herbert@debian.org>  Thu, 18 Apr 2002 19:18:28 +1000

linux-ftpd-ssl (0.17.12+0.3-2) unstable; urgency=low

  * moved from nonus to main

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Sat, 23 Mar 2002 12:18:50 +0100

linux-ftpd-ssl (0.17.12+0.3-1) unstable; urgency=low

  * Fixed REST/STOR combination with OpenBSD patch (#132974).
  * REST now accepts intmax_t (#126766).
  * Built with support for large files (#122961).
  * Added sample limits.conf entry against globbing (#121074).
  * Added Brazilian debconf template (Andre Luis Lopes, #120835).
  * Always specify the syslog facility explicitly (#121644).

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Thu,  7 Mar 2002 09:57:26 +0100

linux-ftpd (0.17-12) unstable; urgency=low

  * Fixed REST/STOR combination with OpenBSD patch (closes: #132974).

 -- Herbert Xu <herbert@debian.org>  Sat,  9 Feb 2002 14:50:45 +1100

linux-ftpd (0.17-11) unstable; urgency=low

  * REST now accepts intmax_t (closes: #126766).

 -- Herbert Xu <herbert@debian.org>  Sun, 20 Jan 2002 19:03:22 +1100

linux-ftpd (0.17-10) unstable; urgency=low

  * Built with support for large files (closes: #122961).

 -- Herbert Xu <herbert@debian.org>  Sun,  9 Dec 2001 17:45:53 +1100

linux-ftpd (0.17-9) unstable; urgency=low

  * Added sample limits.conf entry against globbing (closes: #121074).
  * Added Brazilian debconf template (Andre Luis Lopes, closes: #120835).
  * Always specify the syslog facility explicitly (closes: #121644).

 -- Herbert Xu <herbert@debian.org>  Sat,  1 Dec 2001 18:28:51 +1100

linux-ftpd-ssl (0.17.8+0.3-1) unstable; urgency=low

  * bring in line with linux-ftpd
    * Debconf (see 113611)
    * pam_limits used

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Wed, 21 Nov 2001 16:05:54 +0100

linux-ftpd (0.17-8) unstable; urgency=low

  * Added German debconf template (Sebastian Feltel, closes: #113611).

 -- Herbert Xu <herbert@debian.org>  Sat, 13 Oct 2001 08:13:55 +1000

linux-ftpd (0.17-7) unstable; urgency=low

  * Added debconf about globbing attacks.

 -- Herbert Xu <herbert@debian.org>  Thu, 23 Aug 2001 19:49:01 +1000

linux-ftpd (0.17-6) unstable; urgency=low

  * Register sessions with PAM.
  * Use pam_limits by default.
  * Documented the procedure to counter globbing attacks.

 -- Herbert Xu <herbert@debian.org>  Sat,  9 Jun 2001 13:25:27 +1000

linux-ftpd-ssl (0.17.5+0.3-1) unstable; urgency=low

  * Remove Provides ftpd (closes: #93532)
  * Bring in line with linux-ftpd (see 96640 and 93217)
  * new upstream patch (0.3) brings working -z secure (closes: #92873)
    which can now force ftpd to only accept secure connections
  * add SSL options to manpage (closes: #92602)

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Sun, 13 May 2001 13:44:58 +0200

linux-ftpd (0.17-5) unstable; urgency=low

  * Removed duplicate authentication error message (closes: #96640).

 -- Herbert Xu <herbert@debian.org>  Mon,  7 May 2001 22:08:29 +1000

linux-ftpd (0.17-4) unstable; urgency=low

  * The value of unique is now passed to dataconn (closes: #93217).

 -- Herbert Xu <herbert@debian.org>  Sun, 22 Apr 2001 09:33:26 +1000

linux-ftpd-ssl (0.17.4+0.2-1) unstable; urgency=low

  * bring linux-ftpd updates to -ssl
  * builddepends on libssl-dev

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Sat, 10 Mar 2001 17:48:45 +0100

linux-ftpd (0.17-3) unstable; urgency=low

  * Fixed anonymous authentication bug when PAM is disabled (Liviu Daia,
    Abraham vd Merwe, Rainer Weikusat, closes: #88837).
  * Fixed patterns used to check for existing ftp services (closes: #85579).

 -- Herbert Xu <herbert@debian.org>  Fri,  9 Mar 2001 22:30:37 +1100

linux-ftpd-ssl (0.17.2+0.2-1) unstable; urgency=low

  * initial linux-ftpd-ssl version

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Sun, 18 Feb 2001 14:27:54 +0100

linux-ftpd (0.17-2) unstable; urgency=high

  * Applied bug fix from OpenBSD (closes: #78973).

 -- Herbert Xu <herbert@debian.org>  Thu,  7 Dec 2000 19:47:47 +1100

linux-ftpd (0.17-1) unstable; urgency=low

  * New upstream release (identical to 0.16-2).

 -- Herbert Xu <herbert@debian.org>  Sat, 12 Aug 2000 13:29:38 +1000

linux-ftpd (0.16-2) unstable; urgency=high

  * Fixed a security hole discovered by OpenBSD, thanks to Thomas Roessler for
    notifying me (closes: #66832).
  * Added build-time dependency on debhelper.

 -- Herbert Xu <herbert@debian.org>  Fri,  7 Jul 2000 10:15:38 +1000

linux-ftpd (0.16-1) unstable; urgency=low

  * New upstream release.
  * Added Source-Depends on bison (closes: #61160).
  * Removed checks on the remote address of the data connection which violated
    RFC 959 (closes: #59251).

 -- Herbert Xu <herbert@debian.org>  Mon, 24 Apr 2000 20:46:00 +1000

linux-ftpd (0.11-9) frozen unstable; urgency=low

  * Added Bource-Depends on libpam0g-dev (closes: #49917).

 -- Herbert Xu <herbert@debian.org>  Sat, 18 Mar 2000 12:50:14 +1100

linux-ftpd (0.11-8) unstable; urgency=low

  * Added entry for ~ftp/lib in in.ftpd(8) (closes: #49035).

 -- Herbert Xu <herbert@debian.org>  Sat,  6 Nov 1999 12:21:37 +1100

linux-ftpd (0.11-7) unstable; urgency=low

  * Added missing dependencies on netbase and libpam-modules (closes: #48411).

 -- Herbert Xu <herbert@debian.org>  Wed, 27 Oct 1999 09:25:59 +1000

linux-ftpd (0.11-6) unstable; urgency=low

  * Anonymous users other than ftp should work now (closes: #48252).

 -- Herbert Xu <herbert@debian.org>  Mon, 25 Oct 1999 16:38:16 +1000

linux-ftpd (0.11-5) unstable; urgency=low

  * Installed files according to the FHS.
  * Applied PAM patch from Olaf Kirch.
  * Renamed PAM file to ftp.

 -- Herbert Xu <herbert@debian.org>  Thu,  7 Oct 1999 22:19:58 +1000

linux-ftpd (0.11-4) unstable; urgency=low

  * Rewritten PAM code to remove hack for ftp prompt.
  * Provide/conflict with ftp-server (fixes #42412).

 -- Herbert Xu <herbert@debian.org>  Fri, 10 Sep 1999 10:32:04 +1000

linux-ftpd (0.11-3) unstable; urgency=low

  * Fixed incorrect usage of update-inetd in postinst (fixes #40771).

 -- Herbert Xu <herbert@debian.org>  Mon,  5 Jul 1999 17:46:41 +1000

linux-ftpd (0.11-2) unstable; urgency=low

  * Don't use absoluet paths in PAM configuration file (fixes #38985).

 -- Herbert Xu <herbert@debian.org>  Sat, 19 Jun 1999 12:10:03 +1000

linux-ftpd (0.11-1) unstable; urgency=low

  * Split from netstd.
  * Added support for PAM.
  * Pad with zeros instead of spaces in setproctitle.
  * Add a note about ftp-bugs in README (fixes #29733).
  * Made response to STOU commands conform to RFC 1123 (fixes #32490).

 -- Herbert Xu <herbert@debian.org>  Thu,  1 Apr 1999 13:45:20 +1000
